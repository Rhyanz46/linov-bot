from telegram.ext import Updater, CommandHandler, MessageHandler, Filters
import logging
import os, time
from subprocess import call
update_id = None
import socket
from dotenv import load_dotenv
load_dotenv()

token = os.getenv("TOKEN")

# Enable logging
logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
                    level=logging.INFO)

logger = logging.getLogger(__name__)


# Define a few command handlers. These usually take the two arguments bot and
# update. Error handlers also receive the raised TelegramError object in error.
def start(bot, update):
    """Send a message when the command /start is issued."""
    update.message.reply_text('Hi!')


def help(bot, update):
    """Send a message when the command /help is issued."""
    update.message.reply_text('Help!')

def run_command(bot, update, args):
    hasil = os.popen(" ".join(args)).read()
    update.message.reply_text(hasil)

def echo(bot, update):
    """Echo the user message."""
    if update.message:
        ip = os.popen('wget -qO- http://ipecho.net/plain ; echo').readlines(-1)[0].strip()
        local_ip = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        local_ip.connect(("8.8.8.8", 80))
        local_ip_address = local_ip.getsockname()[0]
        local_ip.close()
        if update.message.text == 'ip' or update.message.text == 'Ip' or update.message.text == 'iP' or update.message.text == 'IP':
            update.message.reply_text(f"IP public : {ip}, IP local : {local_ip_address}")
        elif "anjay" in update.message.text:
            update.message.reply_text("Anjing looo yaaa,, jangan ngomong kasar")
        elif "keren" in update.message.text:
            update.message.reply_text("Bangsat kau.!!!!")
        else :
            update.message.reply_text("maaf, bot ini hanya di gunakan untuk mengecek ip server, balas chat ini dengan `ip` untuk melihat ip")


def error(bot, update, error):
    """Log Errors caused by Updates."""
    logger.warning('Update "%s" caused error "%s"', update, error)


def main():
    """Start the bot."""
    # Create the EventHandler and pass it your bot's token.
    updater = Updater(token)

    # Get the dispatcher to register handlers
    dp = updater.dispatcher

    # on different commands - answer in Telegram
    dp.add_handler(CommandHandler("start", start))
    dp.add_handler(CommandHandler("help", help))
    dp.add_handler(CommandHandler("run", run_command, pass_args=True))

    # on noncommand i.e message - echo the message on Telegram
    dp.add_handler(MessageHandler(Filters.text, echo))

    # log all errors
    dp.add_error_handler(error)

    # Start the Bot
    updater.start_polling()
    print("this work")

    # Run the bot until you press Ctrl-C or the process receives SIGINT,
    # SIGTERM or SIGABRT. This should be used most of the time, since
    # start_polling() is non-blocking and will stop the bot gracefully.
    updater.idle()


if __name__ == '__main__':
    main()
